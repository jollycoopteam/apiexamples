﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Example.Paging.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Example.Paging.Features.Playlist
{
    [Route("api/[controller]")]
    public class PlaylistController : Controller
    {
        private readonly IPageable<Track> service;
        private readonly IUrlHelper urlHelper;

        public PlaylistController(IPageable<Track> service, IUrlHelper urlHelper)
        {
            this.service = service;
            this.urlHelper = urlHelper;
        }

        [HttpGet(Name = "page")]
        public IActionResult Get(PagingParams pagingParams)
        {
            var model = service.GetPaged(pagingParams);

            Response.Headers.Add("X-Pagination", model.GetHeader().ToJson());

            var output = new Output
            {
                Paging = model.GetHeader(),
                Links = GetLinks(model),
                Items = model.List.Select(m => new TrackInfo(m)).ToList()
            };
            return Ok(output);
        }

        private List<LinkInfo> GetLinks(PagedList<Track> model)
        {
            var links = new List<LinkInfo>();

            if (model.HasPreviousPage)
            {
                links.Add(CreateLink("page", model.PreviousPageNumber, model.PageSize, "previousPage", "GET"));

            }

            links.Add(CreateLink("page", model.PageNumber, model.PageSize, "self", "GET"));

            if (model.HasNextPage)
            {
                links.Add(CreateLink("page", model.NextPageNumber, model.PageSize, "nextPage", "GET"));
            }
            return links;
        }

        private LinkInfo CreateLink(string routeName, int pageNumber, int pageSize, string rel, string method)
        {
            return new LinkInfo
            {
                Href = urlHelper.Link(routeName, new
                {
                    PageNumber = pageNumber,
                    PageSize = pageSize
                }),
                Rel = rel,
                Method = method
            };
        }
    }
}
