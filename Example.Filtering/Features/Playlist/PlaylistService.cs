﻿using Example.Services;
using System.Collections.Generic;
using System.Linq;

namespace Example.Filtering.Features.Playlist
{
    public class PlaylistService : IFilterable<Track>
    {
        public ICollection<Track> Tracks { get; set; }

        public PlaylistService()
        {
            Tracks = GetAll();
        }

        public ICollection<Track> GetBy(FilterParams filterParams)
        {
            var query = Tracks.AsQueryable();

            var filterBy = filterParams.FilterBy.Trim().ToLowerInvariant();

            if (!string.IsNullOrEmpty(filterBy))
            {
                query = query
                    .Where(t =>
                    t.Title.ToLowerInvariant().Contains(filterBy)
                    || t.Genre.ToLowerInvariant().Contains(filterBy));
            }

            return query.ToList();
        }

        private ICollection<Track> GetAll()
        {
            return new List<Track>
            {
                new Track()
            };
        }
    }
}
