﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Example.Services;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Example.Filtering.Features.Playlist
{
    [Route("api/[controller]")]
    public class PlaylistController : Controller
    {
        private readonly IFilterable<Track> service;

        public PlaylistController(IFilterable<Track> service)
        {
            this.service = service;
        }

        [HttpGet("filter")]
        public IActionResult Get([FromQuery]FilterParams filterParams)
        {
            var tracks = service.GetBy(filterParams);

            var output = new Output
            {
                Count = tracks.Count,
                Tracks = tracks.Select(m => new TrackInfo(m)).ToList()
            };

            return Ok(output);
        }
    }
}
