﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Example.Services
{
    public interface IFilterable<T>
    {
        ICollection<T> GetBy(FilterParams filterParams);
    }
}
