﻿using System;
using System.Collections.Generic;

namespace Example.Filtering.Features.Playlist
{
    public class Track
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Genre { get; set; }
        public int Minutes { get; set; }

        public override string ToString()
        {
            return $"[{Id}] : Track '{Title}', {Minutes} mins";
        }
    }

    public class Output
    {
        public int Count { get; set; }
        public List<TrackInfo> Tracks { get; set; }
    }

    public class TrackInfo : Track
    {
        public DateTime LastReadAt { get; set; }

        public TrackInfo(Track track)
        {
            this.Id = track.Id;
            this.Genre = track.Genre;
            this.Minutes = track.Minutes;
            this.Title = track.Title;
            this.LastReadAt = DateTime.UtcNow;
        }
    }
}
