﻿using Example.Paging.Features.Playlist;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Example.Paging.Services
{
    public class PlaylistService : IPageable<Track>
    {
        public PlaylistService()
        {
            Tracks = GetAll();
        }

        public ICollection<Track> Tracks { get; }

        public PagedList<Track> GetPaged(PagingParams pagingParams)
        {
            var query = this.Tracks.AsQueryable();

            return new PagedList<Track>(query, pagingParams.PageNumber, pagingParams.PageSize);
        }

        private ICollection<Track> GetAll()
        {
            return null;
        }
    }
}
