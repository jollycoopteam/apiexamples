﻿namespace Example.Paging.Services
{
    public interface IPageable<T>
    {
        PagedList<T> GetPaged(PagingParams pagingParams);
    }
}
